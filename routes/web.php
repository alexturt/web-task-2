<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/groups', 'GroupController@return_group');
Route::post('/groups/create', 'GroupController@create_group')->name('create_group');
Route::delete('/groups/delete/{group}', 'GroupController@delete_group' )->name('delete_group');

Route::get('/students', 'StudentController@return_data');
Route::post('/students/create', 'StudentController@create')->name('create');
Route::post('/students/edit', 'StudentController@edit' )->name('edit');
Route::post('/students/edit_complete', 'StudentController@edit_complete' )->name('edit_complete');
Route::delete('/students/delete/{student}', 'StudentController@delete' )->name('delete');

Route::get('/', function () {
    return view('welcome');
});
