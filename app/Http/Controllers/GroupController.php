<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function return_group()
    {
        $groups = Group::all();
        return view('groups', compact('groups'));
    }
    public function create_group(Request $request)
    {
    	$group = new Group();
        if ($request->name) {
            $group->name = $request->name;
            $group->save();
        }
        return back();
    }
    public function delete_group(Group $group)
    {
    	$group->delete();
    	DB::table('students')->where('group_id', $group->id)->delete();
    	return back();
    }
    
}
