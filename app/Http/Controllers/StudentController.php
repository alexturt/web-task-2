<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Group;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function return_data()
    {
    		$groups = Group::all();
            $students = Student::all();
            $students->load('group');
            return view('index', compact('groups','students'));
    }
    public function create(Request $request) 
    {
    	$student = new Student();
        if ($request->full_name && $request->birthday) {
            $student->full_name = $request->full_name;
            $student->birthday = $request->birthday;
            $student->group_id = $request->group_id;
            $student->save();
        }
    	return back();
	}
	public function edit(Request $request) 
    {
    	$student = new Student();
    	$groups = Group::all();
    	$students = Student::all();
    	foreach ($students as $item) {
    		if ($request->id == $item->id) {
            $student->id = $item->id;
            $student->full_name = $item->full_name;
            $student->birthday = $item->birthday;
            $student->group_id = $item->group_id;
        }
    	}
        
        return view('edit', compact('groups','student'));
	}
	public function edit_complete(Request $request) 
    {
    	if ($request->id) {
            DB::table('students')->where('id', $request->id)->update(['group_id' => $request->group_id,'full_name' => $request->full_name,'birthday' => $request->birthday]);
        }
        return redirect('/students');
	}
	public function delete(Student $student)
    {
        $student->delete();
        return back();
    }
}
