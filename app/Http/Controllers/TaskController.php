<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function Return()
    {
            $tasks = Task::All();
            return view('index', compact('tasks'));
    }
    Public function Create(Request $request) 
    {
    		$newTask = new Task();
    		if (!$request->text) 
    		{
    			return back();
        	}
    		$newTask->task = $request->text;
    		$newTask->Save();
    		return back();
	}
	public function Delete(Task $Task)
    {
        $Task->Delete();
        return back();
    }
}